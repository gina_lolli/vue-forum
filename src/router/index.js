import Vue from 'vue'

import VueRouter from 'vue-router'
import Home from '@/pages/PageHome'
import ThreadShow from '@/pages/PageThreadShow'
import NotFound from '@/pages/PageNotFound'
import Forum from '@/pages/PageForum'
import Category from '@/pages/PageCategory'

Vue.use(VueRouter)

export default new VueRouter({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/thread/:id',
            name: 'ThreadShow',
            component: ThreadShow,
            props: true
        },
        {
            path: '*',
            name: 'NotFound',
            component: NotFound
        },
        {
            path: '/forum/:id',
            name: 'Forum',
            component: Forum,
            props: true
        },
        {
            path: '/category/:id',
            name: 'Category',
            component: Category,
            props: true
        }
    ],
    mode: 'history'
})
